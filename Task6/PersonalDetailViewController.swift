//
//  PersonalDetailViewController.swift
//  Task6
//
//  Created by Sierra 4 on 21/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import ObjectMapper
import SVProgressHUD

class PersonalDetailViewController: UIViewController {
    
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCountry: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var signUp: UIButton!
    @IBOutlet weak var txtPhoneNum: SkyFloatingLabelTextField!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtName.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtName.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtEmail.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtEmail.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtPassword.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtPassword.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtCountry.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtCountry.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtCity.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtCity.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtAddress.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtAddress.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtPhoneNum.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtPhoneNum.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        self.signUp.layer.cornerRadius = self.signUp.frame.height / 2
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func goBackAcion(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        print("sign up")
        var proceed:Bool =  true
        self.signUp.isEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)

        
        if empty(txtName.text!,"username") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        if empty(txtEmail.text!,"email") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        if !Validation.checkEmail(txtEmail.text!) {
            Alert.displayAlert("Invalid email id", self)
            enableSignUpButton()
            closeLoader()
        }
        if empty(txtPassword.text!,"password") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        if empty(txtPhoneNum.text!,"Phone number") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        } else if !Validation.checkPhoneNumber(txtPhoneNum.text!) {
            Alert.displayAlert("Invalid Phone number", self)
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        if empty(txtCountry.text!,"Country") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        if empty(txtCity.text!,"City") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        if empty(txtAddress.text!,"Address") {
            proceed = false
            enableSignUpButton()
            closeLoader()
        }
        
        if proceed == true {
            let signUp:[String: String] = ["username": txtName.text! ,
                          "email": txtEmail.text!,
                          "password": txtPassword.text!,
                          "phone": txtPhoneNum.text!,
                          "country": txtCountry.text!,
                          "city": txtCity.text!,
                          "address": txtAddress.text!,
                          "flag": "1",
                          "birthday": "04/06/1993",
                          "country_code": "001",
                          "postal_code": "100",
                          "country_iso3": "blah--blah",
                          "state": "chandigarh",
                          ]
            
            print(signUp)
            
            ApiHandler.fetchData(urlStr: "signup", parameters: signUp) { (jsonData) in
                let userModel = Mapper<User>().map(JSONObject: jsonData)
                print(userModel?.msg ?? "")
                
                if (userModel?.msg?.isEqual("Email already exists"))! {
                    Alert.displayAlert("Email Already exists !", self)
                    self.enableSignUpButton()
                    self.closeLoader()
                } else if (userModel?.msg?.isEqual("User signed up successfully!"))! {
                    Alert.displayAlert("User signed up successfully!", self)
                    self.enableSignUpButton()
                    self.closeLoader()
                } else {
                    Alert.displayAlert("An error occured !", self)
                    self.enableSignUpButton()
                    self.closeLoader()
                }
            }
            
        }
        
    }
    
    func empty(_ value: String,_ field: String) -> Bool {
        if value.isEmpty {
            Alert.displayAlert("\(field) is empty", self)
            return true
        }
        return false
    }
    
    func enableSignUpButton() {
        self.signUp.isEnabled = true
        return
    }
    
    func closeLoader() {
        SVProgressHUD.dismiss()
    }
    
}
