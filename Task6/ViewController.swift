//
//  ViewController.swift
//  Task6
//
//  Created by Sierra 4 on 21/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import M13Checkbox
import SkyFloatingLabelTextField
import Alamofire
import ObjectMapper
import SVProgressHUD

class ViewController: UIViewController {

    @IBOutlet weak var txtUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var underUsername: UIView!
    @IBOutlet weak var underPassword: UIView!
    
    @IBOutlet weak var checkBox: M13Checkbox!
    
    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //checkBox
        checkBox._IBBoxType = "Square"
        checkBox.cornerRadius = 0
        checkBox.stateChangeAnimation = M13Checkbox.Animation(rawValue: "Fill")!
        
        //username textfield
        txtUsername.selectedTitleColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        txtUsername.lineHeight = 0
        txtUsername.selectedLineHeight = 0
        
        //password textfield
        txtPassword.selectedTitleColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        txtPassword.lineHeight = 0
        txtPassword.selectedLineHeight = 0
        
        //login detail
        if let login = UserDefaults.standard.value(forKey: "login") as? [String: String] {
            txtUsername.text = login["email"]
            txtPassword.text = login["password"]
            if !login.isEmpty {
                checkBox.setCheckState(M13Checkbox.CheckState(rawValue: "Checked")!, animated: true)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.btnSignIn.isEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func signUpAction(_ sender: Any) {
        performSegue(withIdentifier: "signUpPage", sender: nil)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        var proceed: Bool = true
        self.btnSignIn.isEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        
        if (txtUsername.text?.isEmpty)! {
            Alert.displayAlert("Email id empty !", self)
            proceed = false
            closeLoader()
            enableSignInButton()
        }
        
        if !Validation.checkEmail(txtUsername.text!) {
            Alert.displayAlert("Invalid eamil id", self)
            proceed = false
            closeLoader()
            enableSignInButton()
        }
        
        if (txtPassword.text?.isEmpty)! {
            Alert.displayAlert("Password is Empty", self)
            proceed = false
            closeLoader()
            enableSignInButton()
        }
        
        if proceed == true {
            var param:[String: String] = ["email":txtUsername.text!, "password": txtPassword.text!, "flag":"1"]
            
            print("signing in...")
            var details:[String: String] = [:]
            
            ApiHandler.fetchData(urlStr: "login", parameters: param) { (jsonData) in
            
                let userModel = Mapper<User>().map(JSONObject: jsonData)
                print(userModel?.msg ?? "")
                
                if (userModel?.msg ?? "").isEqual("User is not Registered!") {
                    Alert.displayAlert("User not found", self)
                    self.enableSignInButton()
                    self.closeLoader()
                } else if (userModel?.msg ?? "").isEqual("Password Incorrect!") {
                    Alert.displayAlert("Incorrect Password", self)
                    self.enableSignInButton()
                    self.closeLoader()
                } else {
                    details["username"] = userModel?.profile?.username ?? ""
                    details["email"] = userModel?.profile?.email ?? ""
                    details["phone"] = userModel?.profile?.phone ?? ""
                    details["country"] = userModel?.profile?.country ?? ""
                    details["city"] = userModel?.profile?.city ?? ""
                    details["address"] = userModel?.profile?.address ?? ""
                    print(details)
                    self.performSegue(withIdentifier: "LoginPerformed", sender: details)
                }
            }
            
            if checkBox.checkState.rawValue == "Checked" {
                print("checkbox selected")
                UserDefaults.standard.set(param, forKey: "login")
            } else {
                print("checkBox unchecked")
                param.removeAll()
                UserDefaults.standard.set(param, forKey: "login")
            }
        } else {
            enableSignInButton()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginPerformed" {
            let DestViewController = segue.destination as! LoginDetailViewController
            DestViewController.userDetails = sender as? [String: String]
        }
    }
    
    func enableSignInButton() {
        self.btnSignIn.isEnabled = true
        return
    }
    
    func closeLoader() {
        SVProgressHUD.dismiss()
    }
}

