//
//  LoginDetailViewController.swift
//  Task6
//
//  Created by Sierra 4 on 22/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class LoginDetailViewController: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
  
    @IBOutlet weak var stackDetails: UIStackView!
    
    
    var userDetails:[String: String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(userDetails ?? "nil")
        lblName.text = ": " + (userDetails?["username"] ?? "nil")
        lblEmail.text = ": " + (userDetails?["email"] ?? "nil")
        lblPhone.text = ": " + (userDetails?["phone"] ?? "nil")
        lblCountry.text = ": " + (userDetails?["country"] ?? "nil")
        lblCity.text = ": " + (userDetails?["city"] ?? "nil")
        lblAddress.text = ": " + (userDetails?["address"] ?? "nil")
        
        //activity
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
