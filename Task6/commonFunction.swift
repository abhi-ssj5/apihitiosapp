//
//  commonFunction.swift
//  Task6
//
//  Created by Sierra 4 on 21/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Core {
    class func remeberMe(_ loginDetail: [String: String]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UserLogin")
        do {
            let results = try context.fetch(fetchRequest)
            for task in results {
                print(task.value(forKeyPath: "username") as? String ?? "nil")
                print(task.value(forKeyPath: "password") as? String ?? "nil")
                context.delete(task)
            }
            do {
                try context.save()
                print("Previous login deleted")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        let entity  = NSEntityDescription.entity(forEntityName: "UserLogin", in: context)!
        let detail = NSManagedObject(entity: entity, insertInto: context)
        
        detail.setValue(loginDetail["email"], forKeyPath: "username")
        detail.setValue(loginDetail["password"], forKeyPath: "password")
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    class func emptyLogins() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UserLogin")
        do {
            let results = try context.fetch(fetchRequest)
            for task in results {
                print(task.value(forKeyPath: "username") as? String ?? "nil")
                print(task.value(forKeyPath: "password") as? String ?? "nil")
                context.delete(task)
            }
            do {
                try context.save()
                print("Previous login deleted")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func fetchLogin(_ param: inout [String: String]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UserLogin")
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0 {
                param["username"] = results[0].value(forKeyPath: "username") as? String ?? "nil"
                param["password"] = results[0].value(forKeyPath: "password") as? String ?? "nil"
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

class Alert {
    class func displayAlert(_ userMessage: String, _ obj: UIViewController) {
        let alertMessage = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: nil)
        alertMessage.addAction(okAction)
        obj.present(alertMessage, animated: true, completion: nil)
    }
}


class Validation {
    class func checkEmail(_ value: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: value) {
            return false
        }
        return true
    }
    
    class func checkPhoneNumber(_ phone: String) -> Bool {
        let numbers = CharacterSet.decimalDigits
        if String(phone.characters[phone.characters.startIndex]) == "0" {
            return false
        }
        if phone.characters.count > 10 || phone.characters.count < 10 {
            return false
        }
        for number in phone.unicodeScalars {
            if !numbers.contains(number) {
                return false
            }
        }
        return true
    }
    
    func checkName(value: String) -> Bool
    {
        let letters = CharacterSet.letters
        if value.characters.count > 25 || value.characters.count < 5 {
            return false
        }
        for char in value.unicodeScalars {
            if !letters.contains(char) {
                return false
            }
        }
        return true
    }

}











